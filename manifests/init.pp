# Class: kdc
# ===========================
#
# Build a master or slave KDC
#
# Examples
# --------
#
# @example
#    class { 'kdc': }
#
# Authors
# -------
#
# Scotty Logan <swl@stanford.edu>
#
# Copyright
# ---------
#
# Copyright (c) 2016 The Board of Trustees of the Leland Stanford Junior
# University
#
class kdc {
  include kdc::packages
}
