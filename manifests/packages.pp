# == Class: kdc::packages
#
# Installs / updates all the KDC packages
#
# automatically included by kdc module
#
# === Authors
#
# Xueshan Feng <sfeng@stanford.edu>
# Scotty Logan <swl@stanford.edu>
#
# === Copyright
#
# Copyright (c) 2016 The Board of Trustees of the Leland Stanford Junior
# University
#
class kdc::packages (
  $install,
  $uninstall,
){

  include stdlib

  $only_install = difference($install, $uninstall)

  $installing = join($only_install, ' ')
  $uninstalling = join($uninstall, ' ')

  ensure_packages($only_install, { ensure => present })
  ensure_packages($uninstall, { ensure => absent })

  if ($::osfamily == 'Debian') {
    exec { 'apt-mark':
      command => "/usr/bin/apt-mark manual ${installing}",
      require => Package[$only_install],
    }
  }

}
